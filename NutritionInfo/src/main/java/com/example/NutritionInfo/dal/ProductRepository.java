package com.example.NutritionInfo.dal;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {
    ProductEntity findByBarCode (String code);
    boolean existsByBarCode (String code);
    ProductEntity save (ProductEntity product);
}
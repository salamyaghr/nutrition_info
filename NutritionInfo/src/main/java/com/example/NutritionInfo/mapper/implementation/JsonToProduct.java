package com.example.NutritionInfo.mapper.implementation;

import com.example.NutritionInfo.dal.ProductEntity;
import com.example.NutritionInfo.mapper.ProductMapper;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class JsonToProduct implements ProductMapper {

    @Override
    public ProductEntity getProductEntity(JsonNode node){

        JsonNode product = node.get("product");

        return new ProductEntity(
                node.get("code").asText(),
                product.get("ecoscore_data").get("agribalyse").get("name_en").asText());
    }
}

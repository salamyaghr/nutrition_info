package com.example.NutritionInfo.mapper.implementation;

import com.example.NutritionInfo.dal.ProductEntity;
import com.example.NutritionInfo.domain.Nutriments;
import com.example.NutritionInfo.mapper.NutrimentsMapper;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class JsonToNutriments implements NutrimentsMapper {

    @Override
    public Nutriments getNutriments(JsonNode node) {

        JsonNode product = node.get("product");
        JsonNode nutriments = product.get("nutriments");

        return new Nutriments(
                nutriments.get("energy_100g").asInt(),
                nutriments.get("saturated-fat_100g").asInt(),
                nutriments.get("sugars_100g").asInt(),
                nutriments.get("salt_100g").asInt(),
                nutriments.get("fiber_100g").asInt(),
                nutriments.get("proteins_100g").asInt());

    }

}

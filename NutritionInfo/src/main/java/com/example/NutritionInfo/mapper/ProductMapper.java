package com.example.NutritionInfo.mapper;

import com.example.NutritionInfo.dal.ProductEntity;
import com.fasterxml.jackson.databind.JsonNode;

public interface ProductMapper {

    public  ProductEntity getProductEntity(JsonNode node);
}

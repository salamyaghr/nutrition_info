package com.example.NutritionInfo.service.implementation;

import com.example.NutritionInfo.dal.NutritionInfoEntity;
import com.example.NutritionInfo.dal.ProductEntity;
import com.example.NutritionInfo.dal.ProductRepository;
import com.example.NutritionInfo.domain.Nutriments;
import com.example.NutritionInfo.mapper.implementation.JsonToNutriments;
import com.example.NutritionInfo.mapper.implementation.JsonToProduct;
import com.example.NutritionInfo.service.ProductService;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AllArgsConstructor
public class OFFProductService implements ProductService{

    private final ProductRepository productRepository;
    private final OFFDataProvider provider;
    private final OFFNutritionService nutritionService;
    private final JsonToProduct jsonToProduct;
    private final JsonToNutriments jsonToNutriments;

    @Override
    public JsonNode getProduct(String id) throws IOException {
        ProductEntity productEntity;
        if (productRepository.existsByBarCode(id)) {
            productEntity = productRepository.findByBarCode(id);
        }
        else {
            JsonNode node = provider.getProductNode(id);
            productEntity = jsonToProduct.getProductEntity(node);
            Nutriments nutriments = jsonToNutriments.getNutriments(node);
            int score = nutritionService.getScore(nutriments);
            productEntity.setNutritionScore(score);
            NutritionInfoEntity nutritionInfoEntity = nutritionService.getNutritionInfoEntity(score);
            productEntity.setClasse(nutritionInfoEntity.getClasse());
            productEntity.setColor(nutritionInfoEntity.getColor());
            productRepository.save(productEntity);
        }
        return provider.getProductJson(productEntity);
    }
}

package com.example.NutritionInfo.service;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public interface ProductService {
    JsonNode getProduct(String id) throws IOException;
}

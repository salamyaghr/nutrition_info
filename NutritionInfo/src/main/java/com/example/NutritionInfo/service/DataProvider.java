package com.example.NutritionInfo.service;

import com.example.NutritionInfo.dal.ProductEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public interface DataProvider {

    JsonNode getProductNode (String id) throws IOException;
    JsonNode getProductJson (ProductEntity productEntity) throws JsonProcessingException;
}
